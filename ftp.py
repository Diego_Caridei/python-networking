__author__ = 'Guybrush'

import ftplib
ftp =ftplib.FTP("HOST")
try:
    ftp.login("username","password")
    print(ftp.getwelcome())
    ftp.delete("file")
    print(ftp.dir())
    ftp.set_pasv(1)
    ftp.storbinary("STOR  myfile",open("myfile","rb"))
    print(ftp.dir())
except Exception as e:
    print("Exception ",e)
finally:
    ftp.close()
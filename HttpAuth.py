__author__ = 'Guybrush'
import  httplib
import base64
import string

h="host"
u="username"
p="password"
authToken=base64.encodestring('%s%s'%(u,p)).replace('\n','')
print(authToken)

req=httplib.HTTP(h)
req.putrequest("GET","/path")
req.putheader("HOST",h)
req.putheader("Authorization","Basic %s",authToken)
req.send
statusCode, statusMsg,headers =req.getreply()
print("Response", statusMsg)
__author__ = 'Guybrush'
import threading
class myThreadClass(threading.Thread):
    def __init__(self,num,val):
        threading.Thread.__init__(self)
        self.threadNum=num
        self.loopCount=val

    def run(self):
        print("Starting run: ",self.threadNum)
        self.myFunc(self.threadNum,self.loopCount)

    def myFunc(self,num,val):
        count =0
        while count<val:
            print(num," : ",val*count)
            count+=1


t1 = myThreadClass(1,15)
t2=myThreadClass(2,20)
t3=myThreadClass(3,25)
t4=myThreadClass(4,30)


t1.start()
t2.start()
t3.start()
t4.start()

thread=[]
thread.append(t1)
thread.append(t2)
thread.append(t3)
thread.append(t4)

for i in thread:
    i.join()


__author__ = 'Guybrush'
import ssl
import socket
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
ssock=ssl.wrap_socket(s)

try:
    ssock.connect(("www.google.com",443))
    print(ssock.cipher())
except:
    print("error")

try:
    ssock.write(b"GET /\r\n")
except Exception as e :
    print("Write error ", e)

data = bytearray()
try:
    data = ssock.read()
except Exception as e:
    print("Read error ",e)

print(data.decode('utf-8'))

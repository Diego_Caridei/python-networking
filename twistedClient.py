__author__ = 'Guybrush'
from twisted.internet import reactor,protocol

class EchoCLient(protocol.Protocol):
    def connectionMade(self):
        self.transport.write(b"Hello world")

    def dataReceived(self, data):
        print("Server said: ", data)
        self.transport.loseConnection

    def connectionLost(self, reason):
        print("Connection lost")

class EchoFactory(protocol.ClientFactory):
    def clientConnectionFailed(self, connector, reason):
        print("Connection failed")
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print("Connection lost")
        reactor.stop()

def main():
    f= EchoFactory()
    reactor.connectTCP("localhost",8000,f)
    reactor.run()

main()


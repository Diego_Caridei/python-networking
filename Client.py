__author__ = 'Guybrush'
import socket
host='localhost'
sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
addr=(host,5555)
sock.connect(addr)

try:
    msg="Hello world\n"
    sock.sendall(msg)
except socket.errno:
    print("Socket error")
finally:
    sock.close()